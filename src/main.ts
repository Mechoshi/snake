import Rx = require( 'rxjs' );

import '../styles/main.scss';

import {
    Action,
    Segment
} from './app/models';

const SIDE: number = 10;
const WIDTH: number = 400;
const HEIGHT: number = 400;

const canvas: HTMLCanvasElement = document.getElementsByTagName( 'canvas' )[ 0 ];
canvas.width = WIDTH;
canvas.height = HEIGHT;
const context: CanvasRenderingContext2D = canvas.getContext( '2d' );

const infoElement = document.getElementById( 'info' );

const segmentList: Array<Segment> = new Array<Segment>();
const actionList: Array<Action> = new Array<Action>();

let snakesLength = 10;

let currentAction: string = 'right';
let isPaused: boolean = false;
let snakeMustGrow: boolean = false;
let growOnNextMove: boolean = false;

let food: Segment = null;

function drawSegment( segment: Segment, color?: string ): void {
    context.fillStyle = color ? color : '#ffcccc';
    const { x, y, width, height } = segment;
    context.fillRect( x, y, width, height );
}

function drawSnake(): void {
    context.clearRect( 0, 0, WIDTH, HEIGHT );
    segmentList.forEach(( segment: Segment ) => {
        drawSegment( segment );
    } )
}

function initiateTheSnake( initialSize: number, startX: number = 0, startY: number = 0 ): void {
    for ( let i = 0; i < initialSize; i++ ) {
        segmentList.push( new Segment(( startX + i * SIDE ), startY, SIDE, SIDE ) );
        actionList.push( new Action( 'right' ) );
    }
    drawSnake();
}

initiateTheSnake( snakesLength, 0, 100 );

const keypdown$ = Rx.Observable.fromEvent( document, 'keydown' );

keypdown$.subscribe(( event: any ) => {
    let action: string;
    switch ( event.keyCode ) {
        case 38:
            action = 'up';
            break;
        case 40:
            action = 'down';
            break;
        case 37:
            action = 'left';
            break;
        case 39:
            action = 'right';
            break;
        case 80:
            isPaused = !isPaused;
            break;
    }
    if ( action !== undefined && !isPaused ) {
        if ( actionList[ 0 ].direction === 'up' && action === 'down' ) {
            action = 'up';
        }
        if ( actionList[ 0 ].direction === 'down' && action === 'up' ) {
            action = 'down';
        }
        if ( actionList[ 0 ].direction === 'left' && action === 'right' ) {
            action = 'left';
        }
        if ( actionList[ 0 ].direction === 'right' && action === 'left' ) {
            action = 'right';
        }
        currentAction = action;
    }
} )


function movingFunction(): void {
    if ( !isPaused ) {
        let newAction = new Action( currentAction );
        addAction( newAction );
        moveSnake( segmentList, actionList );
        drawSnake();
        if ( !ifSnakeIsAlive( segmentList ) ) {
            clearInterval( intervalFunctionId );
            clearInterval( foodIntervalFunction );
            infoElement.innerHTML = `<span class="snake-over">Snake is Over!</span>`;
        } else {
            infoElement.innerHTML = `<span class="snake-stats">Snake's length is ${ segmentList.length }</span>`;
        }
        if ( food !== null ) {
            drawFood( food, snakeMustGrow ? '#ffff00' : '#ff9900' );
        }

        snakeMustGrow = snakeMustGrow ? snakeMustGrow : snakeEats( segmentList );

        if ( snakeMustGrow && foodReachesTail( segmentList ) ) {
            growSnake( segmentList );
        }
    }
}

function addAction( newAction: Action ): void {
    actionList.unshift( newAction );
    actionList.splice( actionList.length - 1, 1 )
}

function moveSnake( snakeSegments: Array<Segment>, actionList: Array<Action> ): void {
    for ( let i = snakeSegments.length - 1; i >= 0; i-- ) {
        snakeSegments[ i ].move( SIDE, actionList[ snakeSegments.length - i - 1 ].direction );
    }
}

function ifSnakeIsAlive( segmentList: Array<Segment> ): boolean {
    let { centerX, centerY } = segmentList[ segmentList.length - 1 ].getCenter();
    for ( let i = 0; i < segmentList.length - 1; i++ ) {
        let center = segmentList[ i ].getCenter();
        if ( center.centerX === centerX && center.centerY === centerY ) {
            return false;
        }
    }
    return true;
}

function drawFood( food: Segment, color: string ): void {
    drawSegment( food, color );
}

function createFood( snake: Array<Segment> ): Segment {
    let x = Math.floor(( Math.random() * 39 ) + 1 ) * 10;
    let y = Math.floor(( Math.random() * 39 ) + 1 ) * 10;
    let newSegment: Segment;
    let canReturn: boolean = true;
    for ( let segment of snake ) {
        if ( segment.x === x && segment.y ) {
            canReturn = false;
        }
    }
    if ( canReturn ) {
        return newSegment = new Segment( x, y, SIDE, SIDE );
    } else {
        return createFood( snake );
    }
}

function initializeFood( snake: Array<Segment> ): void {
    if ( food === null ) {
        food = createFood( snake );
    }
}

function snakeEats( snake: Array<Segment> ): boolean {
    if ( food === null ) {
        return false;
    }
    let head = snake[ snake.length - 1 ];
    let headCenter = head.getCenter();

    let { centerX, centerY } = food.getCenter();
    if ( headCenter.centerX === centerX && headCenter.centerY === centerY ) {
        return true;
    }
    return false;
}

function foodReachesTail( snake: Array<Segment> ): boolean {
    if ( !snakeMustGrow ) {
        return false;
    }
    let tail = snake[ 0 ];
    let tailCenter = tail.getCenter();
    let { centerX, centerY } = food.getCenter();
    if ( tailCenter.centerX === centerX && tailCenter.centerY === centerY ) {
        return true;
    }
    return false;
}

function growSnake( snake: Array<Segment> ): void {
    let x = snake[ 0 ].x;
    let y = snake[ 0 ].y;
    switch ( actionList[ actionList.length - 1 ].direction ) {
        case 'up':
            y += 10;
            break;
        case 'down':
            y -= 10;
            break;
        case 'left':
            x += 10;
            break;
        case 'right':
            x -= 10;
            break;
    }
    snake.unshift( new Segment( x, y, SIDE, SIDE ) );
    actionList.push( new Action( actionList[ actionList.length - 1 ].direction ) );
    food = null;
    snakeMustGrow = false;
}


let intervalFunctionId = setInterval( movingFunction, 200 );
let foodIntervalFunction = setInterval( initializeFood.bind( null, segmentList ), 10000 );
