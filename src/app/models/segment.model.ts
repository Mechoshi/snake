export class Segment {
    public x: number;
    public y: number;
    public width: number;
    public height: number;

    constructor(
        x: number,
        y: number,
        width: number,
        height: number
    ) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public move( step: number, action: string ) {
        switch ( action ) {
            case 'up':
                this.y -= step;
                this.y = this.y < 0 ? this.y + 400 : this.y;
                break;
            case 'down':
                this.y += step;
                this.y = this.y > 400 ? 0 : this.y;
                break;
            case 'left':
                this.x -= step;
                this.x = this.x < 0 ? 400 : this.x;
                break;
            case 'right':
                this.x += step;
                this.x = this.x > 400 ? 0 : this.x;
                break;
        }
    }

    public getCenter(): any {
        return {
            centerX: this.x + this.width / 2,
            centerY: this.y + this.height / 2
        }
    }
}