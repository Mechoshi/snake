export class Action {
    public direction: string;

    constructor(direction: string) {
        this.direction = direction;
    }
}